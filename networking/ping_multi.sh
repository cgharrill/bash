#!/bin/bash

if [ $# -gt 1 ]
then
	echo "Too many arguments."
elif [ $# == 0 ]
then 
	echo "too few arguments."
#elif [$1 == "-h" ]
#then
#	echo "Enter the first three octets as an argument. The script will ping from .1 to .254 at an interval of 10ms"
else
	for i in {1..254};
	do
		ping -W .01 -c 1 $1.$i | grep "64 bytes"
	done
fi
